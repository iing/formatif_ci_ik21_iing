<?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama  = $data->nama;
  $alamat  = $data->alamat;
	$telp  = $data->telp;
	$tempat_lahir  = $data->tempat_lahir;
	$tanggal_lahir  = $data->tanggal_lahir;
}
     //pisah tanggal bualn tahun
	 $thn_pisah = substr($tanggal_lahir, 0, 4);
	 $bln_pisah = substr($tanggal_lahir, 5, 2);
	 $tgl_pisah = substr($tanggal_lahir, 8, 2);
	?>
<table width="50%" height="78" border="1" align="center">
<tr>
 
  <td colspan="5" align="center" bgcolor="#33FFCC"><b>EDIT MASTER KARYAWAN</td></b>
 
 
  
  </tr>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST">
<table width="50%" border="0" cellspacing="0" cellpadding="5" bgcolor="#999999" align="center">
  <tr>
    <br />  
    <td>nik</td>
    <td>:</td>
    <td>
      <input value="<?= $nik; ?>" type="text" name="nik" id="nik" readonly></td>
  </tr>
  <tr>
    <td>nama</td>
    <td>:</td>
    <td>
      <input value="<?= $nama; ?>" type="text" name="nama" id="nama" maxlength="50"></td>
  </tr>
 <tr>
    <td>alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"  >"<?= $alamat; ?>"</textarea></td>
  </tr>
   <tr>
    <td>telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp"></td>
    </td>
  </tr>
  <tr>
    <td>tempat lahir</td>
    <td>:</td>
    <td> <input value="<?= $tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50"></td>
    </td>
  </tr>
   <tr>
    <td>tanggal lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl; ?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
		?>
       
        <option value="<?=$bln+1;?>" <?= $select_bln; ?>>
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
	    $select_thn = ($thn == $thn_pisah) ? 'selected' : '';

		?>
        <option value="<?=$thn;?>" <?= $select_thn; ?>><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  
  
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>
